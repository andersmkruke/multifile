@echo off
IF NOT "%1"=="run" (
    echo -Build Only-
    GOTO BuildOnly
)

echo -Build and Run-

:BuildOnly
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
set compilerflags=/EHsc
set linkerflags=/out:multifile.exe
cl.exe %compilerflags% @sources @includes /link %linkerflags%

IF NOT "%1"=="run" GOTO End

:Run
echo(
echo Running:
echo ...
multifile.exe

:End