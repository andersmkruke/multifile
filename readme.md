# Multifile C++ Project #
### Practice project for compiling a multifile C++ project from the command line in Windows ###

###### *2018-03-10 Anders Marstein Kruke* ######
---
#### How to compile: ####
+ From the command line, run build.bat
+ Use the optional parameter "run" to run the executable straight after compilation (e.g. > .\build.bat run)